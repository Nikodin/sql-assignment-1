--1
SELECT c.customer_id, c.first_name, c.last_name, c.country, c.postal_code, c.phone, c.email
FROM customer as c

--2
SELECT c.customer_id, c.first_name, c.last_name, c.country, c.postal_code, c.phone, c.email
FROM customer as c 
WHERE c.customer_id = 1

--3
SELECT *
FROM customer
WHERE first_name LIKE '%Johan%'

--4
SELECT c.customer_id, c.first_name, c.last_name, c.country, c.postal_code, c.phone, c.email
FROM customer as c
ORDER BY c.customer_id
LIMIT 7 OFFSET 3;

--5
INSERT INTO customer (first_name, last_name, country, postal_code, phone, email)
VALUES ('Alex', 'Nikodi', 'Nowhere', '1337', '+468888888', 'youwishyouknew@nobrain.com')

--6
UPDATE customer
SET 
first_name = 'Johan', 
last_name = 'Andersson', 
country = 'Sweden',
postal_code = '15533',
phone = '070000000',
email = 'potato@nobrains.com'
WHERE customer_id = 60 

--7
SELECT c.country, COUNT(*)
FROM customer as c
GROUP BY c.country
ORDER BY COUNT(*) DESC;

--8
SELECT first_name, last_name, SUM(inv.total)
FROM customer as cust
INNER JOIN invoice as inv ON inv.customer_id = cust.customer_id
GROUP BY cust.customer_id
ORDER BY SUM DESC

--9
(Trying)






